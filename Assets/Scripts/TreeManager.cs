﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;

public class TreeManager : MonoBehaviour
{
    /*TREE MANAGER component manages:
     * Tree generation
     * Connections between trees (builds a graph)
     * Placement of firecrawler oject that traverse the graph
     * 
     * Fire spread is calculated using graphs and dot product of wind vector against the vector of trees
     */


    public List<(Tree node, List<Node> edges, bool visited)> graphList = new List<(Tree node, List<Node> edges, bool visited)>();
    private List<FireCrawler> fireCrawlerList = new List<FireCrawler>();

    public struct Node
    {
        public Tree tree;
        public float weight;
    }

    //Settings
    [SerializeField]
    private GameObject treePrefab;
    [SerializeField]
    private int numberOfTreesToGenerate;
    [SerializeField]
    private int maximumValidationAttempts;
    [SerializeField]
    private float fireMaxDistance;
    [SerializeField]
    private float fireMinDistance;
    [SerializeField]
    int randomIgniteCount;
    [SerializeField]
    private GameObject fireCrawlerPrefab;


    //Wind values
    private int windDirection = 0;
    private int windSpeed = 0;
    private Vector3 windVector = Vector3.zero;
    [SerializeField]
    private int windStrengthMultiplicator;

    //Terrain values
    [SerializeField]
    private TerrainCollider terrainCollider;
    private Vector3 terrainSize;

    //Tree related values
    private CapsuleCollider treeCollider;
    private float treeHeight;
    private float treeRadius;
    private LayerMask treeLayerMask = 1 << 8;
    private Transform treeContainer;

    // Graph is regenerated when the wind values are updated
    public int WindSpeed {
        get {
            return windSpeed;
        }
        set {
            windSpeed = value * windStrengthMultiplicator;
            windVector = MathHelper.CalculateVector3(windDirection, windSpeed);
            GenerateGraph();
        }
    }

    public int WindDirection {
        get {
            return windDirection;
        }
        set {
            windDirection = value;
            windVector = MathHelper.CalculateVector3(windDirection, windSpeed);
            GenerateGraph();
        }
    }

    private void Start()
    {
        windVector = MathHelper.CalculateVector3(windDirection, windSpeed);
        terrainSize = terrainCollider.bounds.size;
        treeCollider = treePrefab.GetComponent<CapsuleCollider>();
        treeHeight = treeCollider.height;
        treeRadius = treeCollider.radius;
        treeContainer = new GameObject("Trees").transform;
    }


    // Clears existing trees, generates new random trees, generates a graph for fire propagation
    public void GenerateTrees()
    {
        ClearTrees();
        PlaceTrees();
        GenerateGraph();
    }

    // Places the number of trees set in the inspector
    public void PlaceTrees()
    {
        for (int i = 0; i < numberOfTreesToGenerate; i++)
        {
            PlaceTree(GenerateCoord(), true);
        }
    }

    // Removes all trees from the scene
    public void ClearTrees()
    {
        foreach (FireCrawler fire in fireCrawlerList)
        {
            Destroy(fire.gameObject);
        }
        fireCrawlerList.Clear();

        foreach ((Tree node, List<Node> edges, bool visited) treeNode in graphList)
        {
            GameObject treeToDestroy = treeNode.node.gameObject;
            DestroyImmediate(treeNode.node.gameObject);
        }
        DestroyImmediate(treeContainer.gameObject);
        treeContainer = new GameObject("Trees").transform;

        graphList.Clear();
    }

    // Destroys tree, removes tree's graph record, forces neighbors to update their edges
    public void RemoveTree(Tree treeToRemove)
    {
        Collider[] neighbors = GetNeighbors(treeToRemove);
        treeToRemove.gameObject.layer = LayerMask.NameToLayer("Terrain");
        for (int i = 0; i < neighbors.Length; i++)
        {
            Tree neighborTree = neighbors[i].GetComponent<Tree>();
            int edgeIndex = graphList.IndexOf(graphList.Find(x => x.node == neighborTree));
            graphList[edgeIndex] = (neighbors[i].GetComponent<Tree>(), GenerateNode(neighborTree), false);
        }
        graphList.Remove(graphList.Find(x => x.node == treeToRemove));
        Destroy(treeToRemove.gameObject);
    }

    // Places a new tree

    /* IF a position is given - tries to place the tree. 
    * IF a position is NOT given - create random position.

    */
    public void PlaceTree(Vector3 position, bool isInitial)
    {
        Vector3 actualPosition = ValidatePosition(position);
        
        Tree newTree = Instantiate(treePrefab, actualPosition, Quaternion.identity, treeContainer).GetComponent<Tree>();
        //Handles graph population for runtime placed trees
        if (!isInitial)
        {
            List<Node> newNode = GenerateNode(newTree);
            graphList.Add((newTree.gameObject.GetComponent<Tree>(), newNode, false));
            // Force neighboring nodes to regenerate their edges
            Collider[] neighbors = GetNeighbors(newTree);
            for (int i = 0; i < neighbors.Length; i++)
            {
                Tree neighborTree = neighbors[i].GetComponent<Tree>();
                int edgeIndex = graphList.IndexOf(graphList.Find(x => x.node == neighborTree));
                graphList[edgeIndex] = (neighbors[i].GetComponent<Tree>(), GenerateNode(neighborTree), false);
            }
        }
    }

    // Gets the list of potential neighbors within a radius to avoid regenerating the whole graph when a tree is placed or removed during simulation.
    // This prevents each node from analyzing every other node, limiting the node's possible connections to a set radius.
    private Collider[] GetNeighbors(Tree tree)
    {
        //Temporarily disabling collider to ignore self
        tree.gameObject.GetComponent<Collider>().enabled = false;
        Vector3 treePosition = tree.transform.position;
        //Narrow down the list of potential neighbors within a radius. This prevents each node from analyzing every other node, limiting the node's possible connections to a set radius.
        Collider[] neighborTrees = Physics.OverlapSphere(treePosition, fireMaxDistance, treeLayerMask);
        tree.gameObject.GetComponent<Collider>().enabled = true;
        return neighborTrees;
    }

    /* Adjacency list approach was used for building the graph instead of a matrix to save up memory usage for holding the graph, while sacrificing the time complexity some.
     * This approach should be justified for larger graphs like 10000 trees. However, fire propagation is somewhat slowed down because of that.
     * The graph is generated before the simulation begins, as this would reduce the load during runtime, deferring it to pre-simulation setup.
     */
    public void GenerateGraph()
    {
        // Populates graph with lists of targets (edges) for each tree (node)
        Tree[] treeList = treeContainer.gameObject.GetComponentsInChildren<Tree>(false);
        graphList.Clear();
        for (int i = 0; i < treeList.Count(); i++)
        {
            List<Node> node = GenerateNode(treeList[i]);
            graphList.Insert(i, (treeList[i], node, false));
        }
    }

    // Populates the list of neighboring nodes with regards to wind
    private List<Node> GenerateNode(Tree tree) {
        Collider[] neighborTrees = GetNeighbors(tree);
        List<Node> nodes = new List<Node>();
        for (int j = 0; j < neighborTrees.Length; j++)
        {
            Vector3 treePosition = tree.gameObject.transform.position;
            Vector3 heading = neighborTrees[j].transform.position - treePosition;
            // adjusted distance will increase if vectors are pointing in opposite directions
            // MathHelper.cos45 = 0.7 ~= cos(45), substracted for aesthetic reasons of propagation
            float adjustedDistance = heading.magnitude - (Vector3.Dot(heading.normalized, windVector.normalized) - MathHelper.cos45) * windSpeed;
            // adjustedDistance < fireMaxDistance checks if next tree is in range after wind
            // heading.magnitude < fireMinDistance allows to fire trees that are very close despite the wind
            if (adjustedDistance < fireMaxDistance || heading.magnitude < fireMinDistance)
            {
               nodes.Add(new Node { tree = neighborTrees[j].GetComponent<Tree>(), weight = adjustedDistance });
            }
        }
        // sorts the edges of a node according to the weight (distance adjusted to wind) of the edge.
        nodes.Sort((n1, n2) => n1.weight.CompareTo(n2.weight));
        return nodes;
    }

    // Generates random position on the terrain
    public Vector3 GenerateCoord()
    {
        float x = UnityEngine.Random.Range(0f, terrainSize.x);
        float z = UnityEngine.Random.Range(0f, terrainSize.z);
        RaycastHit hit;
        Physics.Raycast(new Vector3(x, 100, z), Vector3.down, out hit);
        return hit.point;
    }

    // Validates if the position is free, adds an offset until it is or the maximum number of attempts is exceeded.
    // If conflicts with another tree - adds offset.
    // Keeps trying to add offset until conflict is resolved or maximum number of attempts is exceeded.
    private Vector3 ValidatePosition(Vector3 position)
    {
        Vector3 actualPosition = position;
        bool validated = false;
        int attemptsCount = 0;
        while (!validated){
            if (Physics.CheckCapsule(actualPosition, actualPosition + new Vector3(0, treeHeight, 0), treeRadius / 4, treeLayerMask)){
                RaycastHit hit;
                actualPosition += Vector3.Scale(UnityEngine.Random.insideUnitSphere * 10, new Vector3(1, 0, 1));
                Physics.Raycast(new Vector3(actualPosition.x, 100, actualPosition.z), Vector3.down, out hit, Mathf.Infinity, ~treeLayerMask, QueryTriggerInteraction.Ignore);
                actualPosition = hit.point;
                attemptsCount++;
            }
            else{
                validated = true;
            }
            if (attemptsCount >= maximumValidationAttempts){
                Debug.LogWarning($"Could not validate tree position after {attemptsCount} tries");
                break;
            }
        }
        return actualPosition;
    }

    // Places fire crawlers that traverse the graph on random trees.
    public void IgniteTree()
    {
        if (graphList.Count > 0){
            for (int i = 0; i < randomIgniteCount; i++){
                int randomIndex = UnityEngine.Random.Range(0, graphList.Count);
                PlaceFire(graphList[randomIndex].node);
            }
        }
    }

    // Instantiates fire crawlers on a provided tree
    public void PlaceFire(Tree tree)
    {
        FireCrawler crawler = GameObject.Instantiate(fireCrawlerPrefab).GetComponent<FireCrawler>();
        fireCrawlerList.Add(crawler);
        crawler.StartTree = tree;
    }

    // Extinguishes a tree that is on fire 
    public void Extinguish(Tree tree)
    {
        tree.StopBurning();
        var node = graphList.Find(x => x.node == tree);
        node.visited = false;
    }

    // Draws all edges between nodes in the graph
    public void DrawDebugGraph()
    {
        if (graphList.Count > 0)
        {
            int edges = 0;
            for (int i = 0; i < graphList.Count; i++)
            {
                Color color = UnityEngine.Random.ColorHSV();
                Vector3 start = graphList[i].node.gameObject.transform.position;
                foreach (Node node in graphList[i].edges){
                    Debug.DrawLine(start, node.tree.gameObject.transform.position, color, 3f);
                    edges += 1;
                }
            }
            print($"Graph contains {graphList.Count} nodes and {edges} edges");
        }
    }

    // Draws all edges of a selected node
    public void DrawNodeEdges(Tree tree) {
        var node = graphList.Find(x => x.node == tree);
        Color color = Color.blue;
        foreach (Node n in node.edges)
        {
            Debug.DrawLine(tree.transform.position, n.tree.gameObject.transform.position, color, 3f);
        }
    }
}
