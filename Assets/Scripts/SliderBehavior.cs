﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
//using UnityEngine.XR.WSA.Input;

public class SliderBehavior : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [System.Serializable]
    public class UnityEventBool : UnityEngine.Events.UnityEvent<float> { }

    private enum SliderType
    {
        Speed,
        Direction
    };

    [SerializeField]
    private SliderType type;
    private TreeManager treeManager;

    private Slider slider;
    private float currValue;
    private float newValue;

    void Start()
    {
        treeManager = FindObjectOfType<TreeManager>();
        slider = gameObject.GetComponent<Slider>();
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData)
    {
        currValue = slider.value;
    }
    void IPointerUpHandler.OnPointerUp(PointerEventData eventData)
    {
        newValue = slider.value;
        if (!Mathf.Approximately(newValue, currValue))
        {
            switch (type)
            {
                case SliderType.Speed:
                    treeManager.WindSpeed = (int)newValue;
                    break;
                case SliderType.Direction:
                    treeManager.WindDirection = (int)newValue;
                    break;
                default:
                    break;
            }
        }
    }
}
