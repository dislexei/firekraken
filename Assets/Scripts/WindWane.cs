﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindWane : MonoBehaviour
{

    private float windSpeed;
    private float windDireciton;
    private LineRenderer lineRenderer;
    Vector3 windVector;

    public float WindSpeed { 
        set {
            windSpeed = value;
            UpdateWindVane(windDireciton, windSpeed);
        } 
    }

    public float WindDirection { 
        set { 
            windDireciton = value;
            UpdateWindVane(windDireciton, windSpeed);
        } 
    }

    private void Start()
    {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        
    }

    private void UpdateWindVane(float newDirection, float newSpeed)
    {
        windVector = MathHelper.CalculateWindVaneDirection((int)newDirection, (int)newSpeed);
        lineRenderer.SetPosition(1, windVector);
    }

}
