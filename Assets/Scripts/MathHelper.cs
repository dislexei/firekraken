﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathHelper
{
    //aesthetic
    public const float cos45 = 0.7f;

    public static Vector3 CalculateVector3(int angle, int magnitude)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle);
        float y = Mathf.Sin(Mathf.Deg2Rad * angle);

        Vector3 vector3 = new Vector3(x, 0, y) * magnitude;

        return vector3;
    }

    public static Vector3 CalculateWindVaneDirection(int angle, int magnitude)
    {
        float x = Mathf.Cos(Mathf.Deg2Rad * angle);
        float y = Mathf.Sin(Mathf.Deg2Rad * angle);

        //aesthetic
        magnitude = (magnitude + 5) *5;
        Vector3 vector3 = new Vector3(x, y, 0) * magnitude;
        return vector3;
    }
}
