﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine;

public class TerrainGenerator : MonoBehaviour
{
    /*This component manages:
     * Wind settings: speed and direction
     * Terrain generation settings: size, sharpness and randomness
     * Fire spread options: maximum distance
     */

    public int width = 128;
    public int length = 128;
    public int depth = 5;
    public bool randomize = true;

    void Start()
    {
        Terrain terrain = gameObject.GetComponent<Terrain>();
        terrain.terrainData = GenerateTerrain(terrain.terrainData);
    }

    TerrainData GenerateTerrain(TerrainData terrainData) {
        terrainData.heightmapResolution = width + 1;
        terrainData.size = new Vector3(width, depth, length) ;
        terrainData.SetHeights(0, 0, GenerateHeights());
        return terrainData;
    }

    float [,] GenerateHeights()
    {
        float random = 0f;
        if (randomize)
        {
            random = UnityEngine.Random.Range(0, 100);
        }

        float[,] heights = new float[width, length];
        for (int i = 0; i < width; i++)
        {
            for (int j = 0; j < length; j++) 
            {
                heights[i, j] = CalculateHeight(i,j, random);
            }
        }
        return heights;

    }

    float CalculateHeight(int x, int y, float random) 
    {
        float xCoord = ((float)x) * 5 / width;
        float yCoord = ((float)y) * 5 / length;
        float height = Mathf.PerlinNoise(xCoord + random, yCoord + random);
        return height;
    }
}
