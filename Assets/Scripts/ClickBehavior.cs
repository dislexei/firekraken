﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ClickBehavior : MonoBehaviour
{
    public enum State { 
        Place,
        Fire,
        Debug
    }

    public State clickState = State.Place;
    public State prevState = State.Place;
    public Camera camera;
    public TreeManager treeManager;

    private void Start()
    {
        camera = GameObject.Find("Main Camera").GetComponent<Camera>();
        treeManager = GameObject.Find("Tree Manager").GetComponent<TreeManager>();
    }

    void Update()
    {   
        //check if pointer is over UI
        if (!IsPointerOverUI())
        {
            if (Input.GetMouseButtonDown(0))
            {
                switch (clickState)
                {
                    case State.Place:
                        PlaceTree(Input.mousePosition);
                        break;
                    case State.Fire:
                        SetFire(Input.mousePosition, true);
                        break;
                    case State.Debug:
                        SelectTree(Input.mousePosition);
                        break;
                    default:
                        break;
                }
                
            }
            if (Input.GetMouseButtonDown(1))
            {
                switch (clickState)
                {
                    case State.Place:
                        RemoveTree(Input.mousePosition);
                        break;
                    case State.Fire:
                        SetFire(Input.mousePosition, false);
                        break;
                    case State.Debug:
                        break;
                    default:
                        break;
                }
             
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                treeManager.PlaceTree(treeManager.GenerateCoord(), false);
            }
        }
    }

    private bool IsPointerOverUI()
    {
        return EventSystem.current.IsPointerOverGameObject();
    }

    void PlaceTree(Vector3 position)
    {
        int layerMask = 1 << 9;
        Ray ray = camera.ScreenPointToRay(position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            treeManager.PlaceTree(hit.point, false);
        }

    }

    void RemoveTree(Vector3 position)
    {
        Ray ray = camera.ScreenPointToRay(position);
        RaycastHit hit;
        int layerMask = 1 << 8;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            treeManager.RemoveTree(hit.transform.gameObject.GetComponent<Tree>());
        }
    }

    void SelectTree(Vector3 position) {
        Ray ray = camera.ScreenPointToRay(position);
        RaycastHit hit;
        int layerMask = 1 << 8;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            treeManager.DrawNodeEdges(hit.transform.gameObject.GetComponent<Tree>());
        }
    }


    void SetFire(Vector3 position, bool isFire) {
        Ray ray = camera.ScreenPointToRay(position);
        RaycastHit hit;
        int layerMask = 1 << 8;
      
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            if (isFire)
            {
                treeManager.PlaceFire(hit.transform.gameObject.GetComponent<Tree>());
            }
            else
            {
                treeManager.Extinguish(hit.transform.gameObject.GetComponent<Tree>());
            }
            
        }
    }

    public void SetState(bool state)
    {
        if (state) clickState = State.Fire;
        else clickState = State.Place;
        prevState = clickState;
        print($"clickState is {clickState}");
    }

    public void EnableDebug(bool debug)
    {
        if (debug) clickState = State.Debug;
        else clickState = prevState;
    }
}
