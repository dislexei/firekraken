﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseButton : MonoBehaviour
{
    [SerializeField]
    private Text label;
    private bool isPaused;

    private void Start()
    {
        isPaused = false;
    }

    public void Pause()
    {
        isPaused = !isPaused;
        if (isPaused)
        {
            label.text = "Simulate";
        }
        else
        {
            label.text = "Pause";
        }
    }

}
