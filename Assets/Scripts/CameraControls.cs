﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControls : MonoBehaviour
{
    public int boundSize = 50; // distance from edge scrolling starts
    public int scrollSpeed = 20;
    private int screenWidth;
    private int screenHeight;
    private Camera camera;

    private int maxFov;
    private int minFov;

     void Start()
        {
            screenWidth = Screen.width;
            screenHeight = Screen.height;
            camera = gameObject.GetComponent<Camera>();
            maxFov = 90;
            minFov = 10;
        }
    void Update()
    {
        // move on +X axis
        if (Input.mousePosition.x > screenWidth - boundSize)
        {
            float newX = transform.position.x + scrollSpeed * Time.deltaTime;
            transform.position = new Vector3(newX, transform.position.y, transform.position.z); 
        }
        // move on -X axis
        if (Input.mousePosition.x < 0 + boundSize)
        {
            float newX = transform.position.x - scrollSpeed * Time.deltaTime;
            transform.position = new Vector3(newX, transform.position.y, transform.position.z); 
        }
        // move on +Z axis
        if (Input.mousePosition.y > screenHeight - boundSize)
        {
            float newZ = transform.position.z + scrollSpeed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, transform.position.y, newZ);
        }
        // move on -Z axis
        if (Input.mousePosition.y < 0 + boundSize)
        {
            float newZ = transform.position.z - scrollSpeed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, transform.position.y, newZ); 
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            
            if(camera.fieldOfView < maxFov)
            {
                camera.fieldOfView += 5;
            }
        }

        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (camera.fieldOfView > minFov)
            {
                camera.fieldOfView -= 5;
            }
        }
    }
}
