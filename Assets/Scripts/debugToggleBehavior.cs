﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UIElements;
using UnityEngine.UI;

public class debugToggleBehavior : MonoBehaviour
{
    [System.Serializable]
    public class UnityEventBool : UnityEngine.Events.UnityEvent<bool> { }
    public UnityEventBool onValueChangedInverse;

    [SerializeField]
    private Button drawGraphButton;


    private Toggle toggle;

    private void Start()
    {
        toggle = gameObject.GetComponent<Toggle>();
        toggle.onValueChanged.AddListener((on) => { onValueChangedInverse.Invoke(!on); });
    }
}
