﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FireCrawler : MonoBehaviour
{
    // A fire crawler is always placed on a tree. It immediately adds this tree to the queue of trees to ignite as the first element.
    private Tree startTree;
    public Tree StartTree{
        set{
            startTree = value;
            StartCoroutine(Propagate());
            treeQueue.Enqueue(startTree);
        }
    }

    public List<(Tree node, List<TreeManager.Node> edges, bool visited)> graphList;
    public Queue<Tree> treeQueue;
    TreeManager treeManager;

    private bool isPaused;
    private Button pauseButton;


    // Fire Crawlers traverse the graph of all trees and ignite their neighbors one by one using a queue
    private void Awake()
    {
        isPaused = false;
        pauseButton = GameObject.Find("Simulate Button").GetComponent<Button>();
        treeManager = GameObject.Find("Tree Manager").GetComponent<TreeManager>();
        graphList = treeManager.graphList;
        treeQueue = new Queue<Tree>();
        pauseButton.onClick.AddListener(OnPause);
    }

    // Switches the paused status
    private void OnPause()
    {
        isPaused = !isPaused;
        print(isPaused);
    }

    // Removes a destroyed tree from the targget queue. Used to avoid null references when a queued tree is destroyed during propagation.
    private void OnTreeDestroyed(object destroyedTree, EventArgs args)
    {
        print("Target tree has been destroyed");
        if (treeQueue.Contains(destroyedTree))
        {
            treeQueue = new Queue<Tree>(treeQueue.Where(x => x != (Tree)destroyedTree));
        }
    }

    // Dequeues trees to ignite with 0.1 second intervals
    private IEnumerator Propagate()
    {
        yield return new WaitForSeconds(0.5f);
        while (treeQueue.Count != 0)
        {
            while (isPaused)
            {
                yield return null;
            }
            IgniteTree(treeQueue.Dequeue());
            yield return new WaitForSeconds(0.1f);
        }
    }

    // Burns the trees and adds its edges to the queue to burn
    private void IgniteTree(Tree tree)
    {
        tree.StartBurning();
        List<Tree> nextTargets = new List<Tree>();
        var node = graphList.Find(x => x.node == tree);

        // get list of unvisited nodes
        if (!node.visited)
        {
            foreach (TreeManager.Node n in node.edges)
            {
                if (n.tree.TreeState == Tree.State.Normal && !n.tree.isMarked)
                {
                    n.tree.isMarked = true;
                    n.tree.OnDestroyEvnt += OnTreeDestroyed;
                    nextTargets.Add(n.tree);
                }
            }
            var temp = node;
            temp.visited = true;
            graphList[graphList.IndexOf(node)] = temp;
        }

        // add target to queue
        for (int i = 0; i < nextTargets.Count; i++)
        {
            if (!treeQueue.Contains(nextTargets[i]))
            {
                treeQueue.Enqueue(nextTargets[i]);
            }
        }
    }
}
