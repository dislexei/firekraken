﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;

public class Tree : MonoBehaviour
{
    public enum State {
        Normal,
        OnFire,
        Burnt
    }

    public bool isMarked = false;

    private State treeState;
    public State TreeState 
    { 
        get 
        {
            return treeState;
        } 
        private set 
        {
            treeState = value; 
        }
    }

    private float timeToBurn;


    //all trees within maximum range of the tree. this will stay constant unless new trees are introduced / existing trees are removed
    //list of potential targets with taking wind into consideration.
    //this will change if the wind changes, and will always be a subset of potential targets above

    [SerializeField]
    private Material normalMaterial;
    [SerializeField]
    private Material burntMaterial;
    [SerializeField]
    private Material onFireMaterial;
    private ParticleSystem particalFire;
    private MeshRenderer meshRenderer;

    private Coroutine burningCoroutine;

    // <summary> Fires an event when the tree is destroyed. This event is subscribed to by the firecrawlers </summary>
    public event EventHandler OnDestroyEvnt;
    private void OnDestroy()
    {
        OnDestroyEvnt?.Invoke(this, EventArgs.Empty);
    }

    void Awake()
    {
        TreeState = State.Normal;
        particalFire = gameObject.GetComponent<ParticleSystem>();
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        timeToBurn = 5f;
    }

    private IEnumerator Burn()
    {
        particalFire.Play();
        TreeState = State.OnFire;
        meshRenderer.material = onFireMaterial;
        yield return new WaitForSeconds(timeToBurn);
        particalFire.Stop();
        TreeState = State.Burnt;
        meshRenderer.material = burntMaterial;
    }

    public void StartBurning()
    {
        if (TreeState == State.Normal)
        {
            burningCoroutine = StartCoroutine(Burn());
        }
    }

    public void StopBurning()
    {
        if(TreeState == State.OnFire)
        {
            StopCoroutine(burningCoroutine);
            StopAllCoroutines();
            particalFire.Stop();
            TreeState = State.Normal;
            meshRenderer.material = normalMaterial;
            isMarked = false;
        }  
    }
}
